# shenc (SHell ENCryptor)

Shell and bash script encryptor and minifer written in Javascript. 

## Function
* Minify the bash or shell script
* Encypt the bash or shell script
* Support all platform (sh, ash, bash, dash, etc.)

## Notice
* Does not support Japanese due to Japanese-Character used as obfuscated character.

## Thanks
* github.com/loilo/node-bash-minifier
* github.com/precious/bash_minifier
* browserify.org
* Prism.js